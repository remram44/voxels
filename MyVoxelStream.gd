extends VoxelStream

const SPHERE_RADIUS = 64
const HALF_SQRT3 = 0.5 * sqrt(3.0)

func emerge_block(buffer: VoxelBuffer, origin: Vector3, lod: int) -> void:
    var block_center = origin + (1 << lod) * Vector3(8, 8, 8)
    var min_dist = SPHERE_RADIUS - HALF_SQRT3 * (1 << lod) * 16
    var max_dist = SPHERE_RADIUS + HALF_SQRT3 * (1 << lod) * 16
    if block_center.length_squared() < min_dist * min_dist:
        buffer.fill(32, 1)
    elif block_center.length_squared() > max_dist * max_dist:
        pass
    else:
        for x in range(16):
            for y in range(16):
                for z in range(16):
                    var pos = origin + Vector3(x + 0.5, y + 0.5, z + 0.5) * (1 << lod)
                    var sq_dist = pos.length_squared()
                    if sq_dist < SPHERE_RADIUS * SPHERE_RADIUS:
                        buffer.set_voxel(32, x, y, z, 1)
