extends KinematicBody

const SENSITIVITY = 0.004
const SPEED = 5.0
const JUMP_SPEED = 10.0
const GRAVITY = 10.0

const CAMERA_DISTANCE = 6.0

onready var camera = $Camera

var pitch = -0.43633
var yaw = 0.0
var velocity = Vector3(0.0, 20.0, 0.0)

func _ready():
    Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _physics_process(delta):
    var move = Vector3()
    if Input.is_action_pressed("move_forward"):
        move.z -= 1.0
    if Input.is_action_pressed("move_back"):
        move.z += 1.0
    if Input.is_action_pressed("move_left"):
        move.x -= 1.0
    if Input.is_action_pressed("move_right"):
        move.x += 1.0
    move.x -= Input.get_action_strength("action_left")
    move.x += Input.get_action_strength("action_right")
    move.z -= Input.get_action_strength("action_forward")
    move.z += Input.get_action_strength("action_back")

    move.x = clamp(move.x, -1.0, 1.0)
    move.z = clamp(move.z, -1.0, 1.0)

    move = transform.basis.xform(move)

    var up_vector = translation.normalized()

    if is_on_floor():
        velocity = SPEED * move  # TODO: rotate for up_vector
        if Input.is_action_just_pressed("jump") and velocity.dot(up_vector) < 0.5 * JUMP_SPEED:
            velocity += JUMP_SPEED * up_vector
    else:
        velocity -= GRAVITY * up_vector * delta

    velocity = move_and_slide(velocity, up_vector)

func _input(event):
    if event is InputEventMouseMotion:
        var motion = event.relative
        yaw -= motion.x * SENSITIVITY
        pitch -= motion.y * SENSITIVITY
        pitch = clamp(pitch, -1.5, 1.5)
        rotation = Vector3(0, yaw, 0)
        camera.translation = Vector3()
        camera.rotation.x = pitch
        camera.translate_object_local(Vector3(0, 0, CAMERA_DISTANCE))
